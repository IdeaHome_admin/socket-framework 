package org.idea.web.socket.handler;

import java.security.Principal;

/**
 * @Author linhao
 * @Date created in 6:11 下午 2021/5/10
 */
public class PrincipalUser implements Principal {

    private String name;

    public PrincipalUser(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
