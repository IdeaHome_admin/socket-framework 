package org.idea.web.socket;

import com.qiyu.core.cache.redis.annotation.CacheDataSource;
import com.qiyu.core.cache.redis.config.RedisEnvConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

/**
 * @Author linhao
 * @Date created in 3:05 下午 2021/5/8
 */
@SpringBootApplication
@Slf4j
@EnableWebSocket
@CacheDataSource(cacheType = RedisEnvConstants.RedisEnvEnum.LOCAL_DB)
public class WebSocketWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebSocketWebApplication.class);
        log.info("================= [WebSocketWebApplication] is started! ==================");
    }
}
