package org.idea.web.socket.handler;

import org.idea.web.socket.handler.PrincipalUser;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.Map;

/**
 * @Author linhao
 * @Date created in 6:16 下午 2021/5/10
 */
@Component
public class PrincipalParamInterceptor extends ChannelInterceptorAdapter {

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
        if (StompCommand.CONNECT.equals(accessor.getCommand())) {
            Object raw = message.getHeaders().get(SimpMessageHeaderAccessor.NATIVE_HEADERS);
            if (raw instanceof Map) {
                Object list = ((Map) raw).get("name");
                if (list instanceof LinkedList) {
                    // 设置当前访问的认证用户
                    accessor.setUser(new PrincipalUser(((LinkedList) list).get(0).toString()));
                }
            }
        }
        return message;
    }
}
