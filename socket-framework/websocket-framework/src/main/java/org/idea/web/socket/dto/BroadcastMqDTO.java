package org.idea.web.socket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author linhao
 * @Date created in 5:07 下午 2021/5/10
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
public class BroadcastMqDTO implements Serializable {

    private Integer eventType;

    private String message;

    private String sessionKey;
}
